# Anny FM: Battery 1,2,3

Originally performed live @ The Loft, Brighton, 4th July 2014.

## Recordings

- [Battery 1,2,3 (Live) on Bandcamp](http://anny.audio/track/battery-123-live)
- [Battery 1,2,3 (Live) on SoundCloud](https://soundcloud.com/anny-fm/battery-123-live)

## Source files

### battery123.tidal

This version updated April 2016 for Tidal 0.7 (jumping three whole major patches! Amazing) to use `cps (125/60)` instead of `bps (125/60)` hence everything becoming prefixed with `cf` - because for some reason, cycles are twice as fast as beats. (Surely it should be the other way round.)

Older versions of this composition with original notes are available on the `original-notes` tag of this repository.

### battery123.megalet.tidal

Contains a big `let` directive to initialize all variables used in `battery123.tidal` (where they are recorded separately). Eval the whole block with `C-c C-e` and dive in.

## Sample attributions

```
arpy bass bass3 hand jvbass peri psr rm tacscan tok trump uxay wobble
```

All of the above from [Dirt-Samples](https://github.com/tidalcycles/Dirt-Samples/tree/c2db9a0dc4ffb911febc613cdb9726cae5175223).

```
warbler
```

I didn't write down the attribution at the time, but I'm fairly certain it's sampled from [Birdsong in Tuscany.wav](http://www.freesound.org/people/evernaut/sounds/37286/) off [Freesound.org](http://www.freesound.org). You did a great job of bookmarking that, past me!
